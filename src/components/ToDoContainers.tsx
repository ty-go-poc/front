import React, {ChangeEvent, Component} from 'react'
import axios from 'axios'
import {Config} from "../config";
import {IBaseApiResponse} from "../types/ApiResponse";
import {IToDoItem, IToDoAllResponse, IToDoAddResponse, IToDoOneResponse} from "../types/Todos";
import {IState} from "../types/State";
import {IProps} from "../types/Props";
import Api from "../api";

const baseUrl: string = Config.api_urls[Config.api_env];

export default class TodosContainer extends Component <IProps, IState>  {
    api: Api;

    constructor(props: IProps) {
        super(props);
        this.state = {
            todos: [],
            resume: undefined,
            content: undefined,
            status: undefined,
            reporter_id: undefined,
            worker_id: undefined,
        };
        this.api = new Api(baseUrl);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.createTodo = this.createTodo.bind(this)
    }

    getTodos() {
        this.api.get(`/tasks`)
            .then(response => {
                const data = response as IToDoAllResponse;
                this.setState({todos: data.toDos || this.state.todos});
            })
            .catch(error => console.log(error))
    }

    handleInputChange(e: ChangeEvent<HTMLInputElement>) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value});
    }

    createTodo(e: any) {
        e.preventDefault();
        e.stopPropagation();
        if (this.state.resume) {
            const newTodo: IToDoItem = {
                resume: this.state.resume,
                content: this.state.content,
                status: this.state.status,
                reporter_id: parseInt(this.state.reporter_id),
                worker_id: parseInt(this.state.worker_id),
            };

            this.api.post(`/tasks`, newTodo)
                .then(response => {
                    const data = response as IToDoAddResponse;
                    axios.get(`${baseUrl}/tasks/${data.id}`).then(
                        response => {
                            const data = response.data as IToDoOneResponse;
                            this.state.todos.push(data.toDo);
                            this.setState({
                                todos: this.state.todos,
                                resume: undefined,
                                content: undefined,
                                status: undefined,
                                reporter_id: undefined,
                                worker_id: undefined,
                            })
                        }
                    ).catch(error => console.log(error));
                })
                .catch(error => console.log(error))
        }
    }

    componentDidMount() {
        this.getTodos()
    }

    render() {
        return (
            <div>
                <div className="inputContainer">
                    <form onSubmit={this.createTodo}>
                        <input
                               type="text"
                               placeholder="Task resume" maxLength={150}
                               name="resume"
                               value={this.state.resume}
                               onChange={this.handleInputChange}
                        />
                        <input
                               type="text"
                               placeholder="Task description"
                               maxLength={150}
                               name="content"
                               value={this.state.content}
                               onChange={this.handleInputChange}
                        />
                        <input
                               type="text"
                               placeholder="Task status"
                               maxLength={150}
                               value={this.state.status}
                               name="status"
                               onChange={this.handleInputChange}
                        />
                        <input
                               type="number"
                               placeholder="Task reporter"
                               maxLength={150}
                               value={this.state.reporter_id}
                               name="reporter_id"
                               onChange={this.handleInputChange}
                        />
                        <input
                               type="number"
                               placeholder="Task worker"
                               maxLength={150}
                               value={this.state.worker_id}
                               name="worker_id"
                               onChange={this.handleInputChange}
                        />
                       <input type="submit" value="Soumet toi" />
                    </form>
                </div>
                <div className="listWrapper">
                    <ul className="taskList">
                        {this.state.todos.map((todo: IToDoItem) => {
                            return(
                                <li className="task" key={todo.id}>
                                    <input className="taskCheckbox" type="checkbox" />
                                    <label className="taskLabel">{todo.resume}</label>
                                    <label className="taskLabel">{todo.content}</label>
                                    <label className="taskLabel">{todo.reporter_id}</label>
                                    <label className="taskLabel">{todo.worker_id}</label>
                                    <label className="taskLabel">{todo.status}</label>
                                    <span className="deleteTaskBtn">x</span>
                                </li>
                            )
                        })}
                    </ul>
                </div>
            </div>
        )
    }
}