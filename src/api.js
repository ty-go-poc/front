/**
 * Class used to handle API calls
 */
export default class Api{
    constructor(url=""){
        this.url = url;
        this.headers = {};
        this.webStorage = window.localStorage || window.sessionStorage;
        this.log_errors = true;
        this.cookies = true;
    }

    enableCookies() {
        this.cookies = true;
    }

    /**
     * Will add headers to instance
     *
     * @param {*} data
     */
    addHeaders(data = {}){
        for(let h in data){
            if (data.hasOwnProperty(h))
                this.headers[h] = data[h];
        }
    }

    /**
     * Will replace instance's headers
     * @param {*} data
     */
    setHeaders(data = null){
        this.headers = data;
    }

    getFetchParams(method, data, opt={}){
        // Base header construction
        this.addHeaders({"Content-Type": "application/json"});
        let _headers = new Headers(this.headers);


        // Request options, method, headers and body if needed
        let options = {
            method: method,
            headers: _headers
        };

        if(opt.redirect)
            options.redirect = opt.redirect;

        // if (this.cookies)
        //     options.credentials = "include";

        /*
         * Creating body if needed
         */
        //
        //     let formData = new FormData();
        //     Object.keys(data).forEach(key => formData.append(key, data[key]));
        //     options.body = formData;
        // }
        if(data !== null) {
            options.body = JSON.stringify(data);
        }

        return options;
    }

    /**
     *
     * @param {string} method POST, GET, PUT, DELETE
     * @param {string} endpoint
     * @param {*} data POST data if needed empty for no body
     */
    _fetch(method, endpoint, data=null, opt={}){
        let options = this.getFetchParams(method, data, opt);

        let response_type = 'json';
        if(opt.response_type)
            response_type = opt.response_type;

        /**
         * Main call
         */
        return fetch(this.url + endpoint, options)
            .then(res => {
                if(!res.ok || (res.status !== 200 && res.status !== 201))
                    return res.json().then(e => {throw e;});

                if(response_type === 'json')
                    return res.json();
                else if(response_type === 'blob')
                    return res.blob();
            })
            .then( res => { return Promise.resolve(res); })
            .catch(e => {
                if(this.log_errors)
                    console.log(e);
                throw e;
            });
    }

    get(endpoint, response_type='json', options={}){
        return this._fetch("GET", endpoint, null, Object.assign(options, {response_type: response_type}));
    }

    del(endpoint, data){
        return this._fetch("DELETE", endpoint, data);
    }

    post(endpoint, data, options={}){
        return this._fetch("POST", endpoint, data, options);
    }

    put(endpoint, data){
        return this._fetch("PUT", endpoint, data);
    }

    download(endpoint, filename, data=null, option={}){
        let method = "GET";
        if(option.method)
            method = option.method;

        return this._fetch(method, endpoint, data, {
            response_type: 'blob'
        })
            .then(blob => {
                let url = window.URL.createObjectURL(blob);
                let a = document.createElement('a');
                a.href = url;
                a.download = filename;
                document.body.appendChild(a);
                a.click();
                setTimeout(() => {
                    document.body.removeChild(a);
                    window.URL.revokeObjectURL(url);
                }, 100);
            })
            .catch(e => {
                throw e;
            });
    }

    async getDataUri(blob, callback) {
        return await new Promise((resolve, reject) => {
            let reader = new FileReader();
            reader.readAsDataURL(blob);
            reader.onload = () => {
                resolve(reader.result);
            };
        });
    }
}
