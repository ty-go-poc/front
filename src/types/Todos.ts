import {IUser} from "./User";
import {IBaseApiResponse} from "./ApiResponse";

export interface IToDoItem {
    id?: number
    resume: string
    content?: string
    status?: string
    reporter_id?: number
    worker_id?: number
    worker?: IUser
    reporter?: IUser
}


export interface IToDoAllResponse extends IBaseApiResponse {
    toDos?: Array<IToDoItem>
}

export interface IToDoOneResponse extends IBaseApiResponse {
    toDo?: IToDoItem
}

export interface IToDoAddResponse extends IBaseApiResponse {
    id: number
}