export interface IConfig {
    api_env: string,
    api_urls: {
        [key: string]: string
    },
    [key: string]: string|number|object
}

export const Config:IConfig = {
    api_env: "local",
    api_urls: {
        local: "http://localhost:8080/v1"
    }
}